package com.example.myapplication.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.ColorInt
import androidx.fragment.app.Fragment
import com.example.myapplication.R
import com.example.myapplication.databinding.FragmentActivityBinding

private val TAG = ActivityFragment::class.java.simpleName

private const val COLOR_PARAM = "COLOR_PARAM"
private const val TEXT_PARAM = "TEXT_PARAM"

class ActivityFragment : Fragment(R.layout.fragment_activity) {
    private var textParam: String? = null
    private var colorParam: Int? = null

    private var _binding: FragmentActivityBinding? = null

    private val binding: FragmentActivityBinding
        get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            textParam = it.getString(TEXT_PARAM)
            colorParam = it.getInt(COLOR_PARAM)
        }

        savedInstanceState?.let {
            textParam = it.getString(TEXT_PARAM)
            colorParam = it.getInt(COLOR_PARAM)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentActivityBinding.inflate(inflater, container, false)
//        if (savedInstanceState == null) {
//            binding.identifiedTextView.text = textParam
//            binding.root.setBackgroundResource(colorParam!!)
//        } else {
//            binding.identifiedTextView.text = savedInstanceState.getString(TEXT_PARAM)
//            binding.root.setBackgroundResource(savedInstanceState.getInt(COLOR_PARAM))
//        }

        binding.identifiedTextView.text = textParam
        binding.root.setBackgroundResource(colorParam!!)

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.apply {
            putString(TEXT_PARAM, textParam)
            putInt(COLOR_PARAM, colorParam!!)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(text: String, @ColorInt color: Int) = ActivityFragment().apply {
            arguments = Bundle().apply {
                putString(TEXT_PARAM, text)
                putInt(COLOR_PARAM, color)
            }
        }

    }
}