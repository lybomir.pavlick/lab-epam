package com.example.myapplication

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.myapplication.databinding.ActivityMainBinding
import com.example.myapplication.ui.ActivityFragment

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    // comment
    @SuppressLint("ResourceAsColor")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .setReorderingAllowed(true)
                .add(R.id.top_fragment_container_view, ActivityFragment.newInstance("Top Fragment", TOP_FRAGMENT_COLOR), TOP_FRAGMENT_TAG)
                .add(R.id.bottom_fragment_container_view, ActivityFragment.newInstance("Bottoms Fragment", BOTTOM_FRAGMENT_COLOR), BOTTOM_FRAGMENT_TAG)
                .commit()
        }
    }

    companion object {
        const val TOP_FRAGMENT_TAG = "Top"
        const val BOTTOM_FRAGMENT_TAG = "Bottom"

        const val TOP_FRAGMENT_COLOR = R.color.green
        const val BOTTOM_FRAGMENT_COLOR = R.color.blue
    }
}