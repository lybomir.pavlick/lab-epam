package com.example.myapplication.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.myapplication.MainActivity
import com.example.myapplication.R
import com.example.myapplication.databinding.FragmentNavBinding

private val TAG = NavFragment::class.java.simpleName

class NavFragment : Fragment(R.layout.fragment_nav) {

    private var _binding: FragmentNavBinding? = null
    private var colorFlag = true

    private val binding: FragmentNavBinding
        get() = _binding!!

    private val clickListener = View.OnClickListener { p0 ->
        when (p0!!.id) {
            R.id.swap_colors_btn -> {
                Log.d(TAG, "Swap Colors Button was clicked...")

                parentFragmentManager.apply {
                    colorFlag = if (colorFlag) {
                        findFragmentByTag(MainActivity.TOP_FRAGMENT_TAG)!!.requireView()
                            .setBackgroundResource(MainActivity.BOTTOM_FRAGMENT_COLOR)
                        findFragmentByTag(MainActivity.BOTTOM_FRAGMENT_TAG)!!.requireView()
                            .setBackgroundResource(MainActivity.TOP_FRAGMENT_COLOR)
                        false
                    } else {
                        findFragmentByTag(MainActivity.TOP_FRAGMENT_TAG)!!.requireView()
                            .setBackgroundResource(MainActivity.TOP_FRAGMENT_COLOR)
                        findFragmentByTag(MainActivity.BOTTOM_FRAGMENT_TAG)!!.requireView()
                            .setBackgroundResource(MainActivity.BOTTOM_FRAGMENT_COLOR)
                        true
                    }
                }
            }
            R.id.swap_fragments_btn -> {
                Log.d(TAG, "Swap Fragments Button was clicked...")
                with(parentFragmentManager) {
                    val topFragment = findFragmentByTag(MainActivity.TOP_FRAGMENT_TAG)
                    val bottomFragment = findFragmentByTag(MainActivity.BOTTOM_FRAGMENT_TAG)

                    val topState = saveFragmentInstanceState(topFragment!!)
                    val bottomState = saveFragmentInstanceState(bottomFragment!!)

                    beginTransaction()
                        .remove(topFragment)
                        .remove(bottomFragment)
                        .add(R.id.bottom_fragment_container_view, ActivityFragment().apply {
                            setInitialSavedState(topState)
                        }, MainActivity.BOTTOM_FRAGMENT_TAG)
                        .add(R.id.top_fragment_container_view, ActivityFragment().apply {
                            setInitialSavedState(bottomState)
                        }, MainActivity.TOP_FRAGMENT_TAG)
                        .commit()
                    colorFlag = !colorFlag
                }

            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentNavBinding.inflate(inflater, container, false)
        with(binding) {
            swapColorsBtn.setOnClickListener(clickListener)
            swapFragmentsBtn.setOnClickListener(clickListener)
        }
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        @JvmStatic
        fun newInstance() = NavFragment()
    }
}